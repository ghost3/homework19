#include <iostream>

class Animal
{
public:

    virtual void Voice()
    {
        std::cout << "*sounds of abstract animal*\n";
    }
};

class Dog : public Animal
{
public:

    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:

    void Voice() override
    {
        std::cout << "Meow!\n";
    }
};

class Duck : public Animal
{
public:

    void Voice() override
    {
        std::cout << "Quack!\n";
    }
};

class Pig : public Animal
{
public:

    void Voice() override
    {
        std::cout << "Oink!\n";
    }
};

class FrogInABlender: public Animal
{
public:

    void Voice() override
    {
        std::cout << "Wzzhhh!\n";
    }
};


int main()
{
    Dog* dog = new Dog;
    Cat* cat = new Cat;
    Pig* pig = new Pig;
    FrogInABlender* pushTheButton = new FrogInABlender;

    Animal* animals[4] = {dog, cat, pig, pushTheButton};

    for (int i = 0; i < 4; ++i)
        animals[i]->Voice();
}

